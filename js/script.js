let createNewUser=(name,surname)=>{
    return {
        firstName:name,
        lastName:surname,
        getLogin: function () {
            return this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName: function(name2) {
            this.firstName=name2;
        },
        setLastName: function(surname2) {
            this.lastName=surname2;
        }
    }

    return newUser;
}
const newUser=createNewUser("Ivan",'Kravchenko');
console.log(newUser);
console.log(newUser.getLogin());
